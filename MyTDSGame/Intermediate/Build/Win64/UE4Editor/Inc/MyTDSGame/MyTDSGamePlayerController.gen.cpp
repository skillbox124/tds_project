// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MyTDSGame/Game/MyTDSGamePlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyTDSGamePlayerController() {}
// Cross Module References
	MYTDSGAME_API UClass* Z_Construct_UClass_AMyTDSGamePlayerController_NoRegister();
	MYTDSGAME_API UClass* Z_Construct_UClass_AMyTDSGamePlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_MyTDSGame();
// End Cross Module References
	void AMyTDSGamePlayerController::StaticRegisterNativesAMyTDSGamePlayerController()
	{
	}
	UClass* Z_Construct_UClass_AMyTDSGamePlayerController_NoRegister()
	{
		return AMyTDSGamePlayerController::StaticClass();
	}
	struct Z_Construct_UClass_AMyTDSGamePlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyTDSGamePlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_MyTDSGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyTDSGamePlayerController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Game/MyTDSGamePlayerController.h" },
		{ "ModuleRelativePath", "Game/MyTDSGamePlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyTDSGamePlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyTDSGamePlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyTDSGamePlayerController_Statics::ClassParams = {
		&AMyTDSGamePlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyTDSGamePlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyTDSGamePlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyTDSGamePlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyTDSGamePlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyTDSGamePlayerController, 3896811914);
	template<> MYTDSGAME_API UClass* StaticClass<AMyTDSGamePlayerController>()
	{
		return AMyTDSGamePlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyTDSGamePlayerController(Z_Construct_UClass_AMyTDSGamePlayerController, &AMyTDSGamePlayerController::StaticClass, TEXT("/Script/MyTDSGame"), TEXT("AMyTDSGamePlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyTDSGamePlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
