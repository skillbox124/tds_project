// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYTDSGAME_MyTDSGamePlayerController_generated_h
#error "MyTDSGamePlayerController.generated.h already included, missing '#pragma once' in MyTDSGamePlayerController.h"
#endif
#define MYTDSGAME_MyTDSGamePlayerController_generated_h

#define MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_SPARSE_DATA
#define MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_RPC_WRAPPERS
#define MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyTDSGamePlayerController(); \
	friend struct Z_Construct_UClass_AMyTDSGamePlayerController_Statics; \
public: \
	DECLARE_CLASS(AMyTDSGamePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyTDSGame"), NO_API) \
	DECLARE_SERIALIZER(AMyTDSGamePlayerController)


#define MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMyTDSGamePlayerController(); \
	friend struct Z_Construct_UClass_AMyTDSGamePlayerController_Statics; \
public: \
	DECLARE_CLASS(AMyTDSGamePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyTDSGame"), NO_API) \
	DECLARE_SERIALIZER(AMyTDSGamePlayerController)


#define MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyTDSGamePlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyTDSGamePlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyTDSGamePlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyTDSGamePlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyTDSGamePlayerController(AMyTDSGamePlayerController&&); \
	NO_API AMyTDSGamePlayerController(const AMyTDSGamePlayerController&); \
public:


#define MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyTDSGamePlayerController(AMyTDSGamePlayerController&&); \
	NO_API AMyTDSGamePlayerController(const AMyTDSGamePlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyTDSGamePlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyTDSGamePlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyTDSGamePlayerController)


#define MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_9_PROLOG
#define MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_SPARSE_DATA \
	MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_RPC_WRAPPERS \
	MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_INCLASS \
	MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_SPARSE_DATA \
	MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_INCLASS_NO_PURE_DECLS \
	MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYTDSGAME_API UClass* StaticClass<class AMyTDSGamePlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyTDSGame_Source_MyTDSGame_Game_MyTDSGamePlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
