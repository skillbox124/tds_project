// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMovementState : uint8;
#ifdef MYTDSGAME_MyTDSGameCharacter_generated_h
#error "MyTDSGameCharacter.generated.h already included, missing '#pragma once' in MyTDSGameCharacter.h"
#endif
#define MYTDSGAME_MyTDSGameCharacter_generated_h

#define MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_SPARSE_DATA
#define MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAxisY); \
	DECLARE_FUNCTION(execInputAxisX);


#define MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAxisY); \
	DECLARE_FUNCTION(execInputAxisX);


#define MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyTDSGameCharacter(); \
	friend struct Z_Construct_UClass_AMyTDSGameCharacter_Statics; \
public: \
	DECLARE_CLASS(AMyTDSGameCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyTDSGame"), NO_API) \
	DECLARE_SERIALIZER(AMyTDSGameCharacter)


#define MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAMyTDSGameCharacter(); \
	friend struct Z_Construct_UClass_AMyTDSGameCharacter_Statics; \
public: \
	DECLARE_CLASS(AMyTDSGameCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyTDSGame"), NO_API) \
	DECLARE_SERIALIZER(AMyTDSGameCharacter)


#define MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyTDSGameCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyTDSGameCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyTDSGameCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyTDSGameCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyTDSGameCharacter(AMyTDSGameCharacter&&); \
	NO_API AMyTDSGameCharacter(const AMyTDSGameCharacter&); \
public:


#define MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyTDSGameCharacter(AMyTDSGameCharacter&&); \
	NO_API AMyTDSGameCharacter(const AMyTDSGameCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyTDSGameCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyTDSGameCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyTDSGameCharacter)


#define MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(AMyTDSGameCharacter, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AMyTDSGameCharacter, CameraBoom); }


#define MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_11_PROLOG
#define MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_SPARSE_DATA \
	MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_RPC_WRAPPERS \
	MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_INCLASS \
	MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_SPARSE_DATA \
	MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_INCLASS_NO_PURE_DECLS \
	MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYTDSGAME_API UClass* StaticClass<class AMyTDSGameCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyTDSGame_Source_MyTDSGame_Character_MyTDSGameCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
