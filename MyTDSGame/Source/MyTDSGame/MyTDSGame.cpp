// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTDSGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MyTDSGame, "MyTDSGame" );

DEFINE_LOG_CATEGORY(LogMyTDSGame)
 