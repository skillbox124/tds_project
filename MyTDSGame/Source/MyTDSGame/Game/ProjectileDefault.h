// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "../FunctionLibrary/Types.h"
#include "ProjectileDefault.generated.h"

USTRUCT()
struct FProjectileInfo
{
	GENERATED_BODY()
};

UCLASS()
class MYTDSGAME_API AProjectileDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectileDefault();

	//Creating base of projectile.

		//Adding Menu for adding mesh
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* BulletMesh = nullptr;

	// adding SphereComponnent whic works like collison sphere. Doesn't exist in game if not added in cpp
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USphereComponent* BulletColisionSphere = nullptr;

	//Adding ProjectilemovementCopmonent
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UProjectileMovementComponent* BulletProjectileMovement = nullptr;

	//Adding ParticleSystem
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UParticleSystemComponent* BulletFX = nullptr;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileLogic")
	FProjectileInfo ProjectileSettings;



	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Creating funtion so that AddDynamic works? ��������� ������������ ���������� � ������ �������?
	UFUNCTION(BlueprintCallable)
	void BulletCollisionSphereHit (class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	//Creating funcion so that BeginOverlapWorks?
	UFUNCTION(BlueprintCallable)
	void BulletCollisionSphereBeginOverlap (class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	//Creatin function so that EndOverpalWorks
	UFUNCTION(BlueprintCallable)
	void BulletCollisionSphereEndOverlap (class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
