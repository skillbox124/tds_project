// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTDSGameGameMode.h"
#include "MyTDSGamePlayerController.h"
#include "../Source/MyTDSGame/Character/MyTDSGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyTDSGameGameMode::AMyTDSGameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMyTDSGamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("Blueprint'/Game/BluePrints/Character/MyTDSCharacter'"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
